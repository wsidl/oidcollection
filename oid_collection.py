import filters
import collections
from enum import Enum, auto
from multiprocessing import RLock

filters.add_operator('exists', (object,), None)
filters.add_operator('!exists', (object,), None)

COLLECTION_SEPARATOR = "/"
FIELD_SEPARATOR = "."


class _Empty(str):
	pass


Empty = _Empty()


class FieldAction(Enum):
	add = auto()
	update = auto()
	delete = auto()
	search = auto()
	list = auto()


def _parse_values(key, input_value, action, search_key=Empty, old_value=Empty, new_value=Empty):
	delete_value = set()

	# check if adding a value
	if action == FieldAction.add:
		k = '' if key is None else key
		if search_key.startswith(k):
			new_k = search_key[len(k):]
			if FIELD_SEPARATOR not in new_k:
				input_value[new_k] = new_value
				return

	def handle_items(_field_key, _inner_key):
		for _returned_key, _value in _parse_values(
				_field_key, input_value[_inner_key], action, search_key, old_value, new_value):
			if action in [FieldAction.search, FieldAction.list]:
				yield _returned_key, _value
			else:
				if _value is True:
					if action == FieldAction.update:
						input_value[_field_key] = new_value
						return
					delete_value.add(_value)

	# Test input_value type and handle as needed
	if isinstance(input_value, dict):
		for field_key in input_value:
			_key = ('' if key is None else key + FIELD_SEPARATOR) + field_key
			for a, b in handle_items(_key, field_key):
				yield a, b

	elif isinstance(input_value, (list, set)):
		_key = ('' if key is None else key + FIELD_SEPARATOR + '*')
		for field_index in range(len(input_value)):
			for a, b in handle_items(_key, field_index):
				yield a, b
	else:
		# Non-iterable type, should handle value
		if action == FieldAction.list or (
				action == FieldAction.search and search_key == key):
			yield key, input_value
		elif action in [FieldAction.delete, FieldAction.change]:
			yield None, search_key == key and old_value == input_value
		return

	# Delete Values
	for k in delete_value:
		if isinstance(input_value, dict):
			del input_value[k]
		elif isinstance(input_value, (set, list)):
			input_value.remove(k)


def get_key_value(input_dict, search_key=Empty, old_value=Empty, new_value=Empty):
	if search_key == Empty:
		# Return all values
		action = FieldAction.list
	else:
		if old_value == Empty:
			action = FieldAction.search if new_value == Empty else FieldAction.add
		else:
			action = FieldAction.delete if new_value == Empty else FieldAction.update
	for a, b in _parse_values(None, input_dict, action, search_key, old_value, new_value):
		yield a, b


def set_key_value(input_dict, search_key=Empty, old_value=Empty, new_value=Empty):
	_ = [a for a in get_key_value(input_dict, search_key, old_value, new_value)]


class IndexedObject(dict):
	def __init__(self, data, collection=None, index=None):
		super(IndexedObject, self).__init__(
			**(data if isinstance(data, dict) else dict(data.items())))
		self.__id = index
		self.__parent = collection
		self.__current_values = list(get_key_value(data))
		self.__listed_keys = set([x[0] for x in self.__current_values])

	@property
	def id(self):
		return self.__id if self.__parent is None else \
			"{}{}{}".format(self.__parent.name, COLLECTION_SEPARATOR, self.__id)

	def get(self, field, default=Empty):
		t = [x for _, x in get_key_value(self, field)]
		t = t[0] if len(t) == 1 else t
		if len(t) == 0:
			if default == Empty:
				raise KeyError("Given Field Pattern returned no value")
			return default
		return t

	def add_field(self, field, new_value):
		self._update(field, new_value=new_value)

	def delete_field(self, field, current_value):
		self._update(field, old_value=current_value)

	def update_field(self, field, new_value, current_value=Empty):
		self._update(
			field,
			current_value if current_value != Empty else (
				self.get(field) if field in self.__listed_keys else Empty),
			new_value
		)

	def _update(self, field, old_value=Empty, new_value=Empty):
		set_key_value(self, field, old_value, new_value)
		try:
			if self.__parent is not None:
				self.__parent.update_indexes_for_object(self)
			self.__current_values = list(get_key_value(self))
			self.__listed_keys = set([x[0] for x in self.__current_values])
		except UniqueIndexException as e:
			# Reset value
			set_key_value(self, field, new_value, old_value)
			raise e


class OIDException(Exception):
	pass


class UniqueIndexException(OIDException):
	pass


def generate_field_values(input_object, _prefix=None):
	"""Converts nested dictionaries and properties into a 2-value tuple list

	Args:
		input_object: Object to extrapolate values
		_prefix (str): Prefix to attach to field names in recursive iterations

	Returns:
		(str, any)[]: List of 2-value tuples with their associated values
	"""
	values = []
	if isinstance(input_object, dict) or (hasattr(input_object, 'keys') and hasattr(input_object, 'get')):
		for att in input_object.keys():
			new_prefix = ('' if _prefix is None else _prefix + FIELD_SEPARATOR) + att
			val = input_object.get(att)
			values += generate_field_values(val, new_prefix)
	elif isinstance(input_object, (list, tuple, set)) or hasattr(input_object, '__iter__'):
		new_prefix = ('' if _prefix is None else _prefix + FIELD_SEPARATOR) + '*'
		for value in input_object:
			values += generate_field_values(value, new_prefix)
	else:
		values.append((_prefix, input_object))
	return values


class CollectionType(object):
	def __init__(self, name=None, parent=None):
		"""

		Args:
		"""
		self._collections = {}
		self.__name = name
		self.__parent = parent
		self.__indexes = {}

	@property
	def name(self):
		""" Name of the collection in a hierarchy

		Returns:
			str:
		"""
		return self.__name

	def get(self, object_id):
		""" Retrieve a specific object by OID

		Args:
			object_id (str): Object ID Reference

		Returns:
			IndexedObject: Object contained in collection referred by `object_id`
		"""
		raise NotImplementedError

	def is_unique(self, field):
		""" Check validity of current field and identifies if the field currently is unique

		This helps in establishing a new Unique Field criteria before it's been placed

		Args:
			field (str): Field name/pattern for the collection

		Returns:
			bool:
		"""
		raise NotImplementedError

	def find(self, *queries):
		""" Performs a query of the contained objects to return any objects that match the given query

		Args:
			*queries (str[]): Query Parameters to perform

		Returns:
			IndexedObject[]: List of objects that apply to the given queries
		"""
		raise NotImplementedError


class IndexedCollection(CollectionType):
	def __init__(self, parent=None, collection_name=None):
		super(IndexedCollection, self).__init__(parent, collection_name)
		self.__indexes = collections.defaultdict(lambda: collections.defaultdict(set))
		self.__unique_fields = set()
		self.__other_fields = set()
		self.__iter_idx = None
		self.__iter_keys = None
		self.__max_id = 0
		self.__lock = RLock()

	def update_indexes_for_object(self, indexed_object, remove=False):
		""" Updates internal indexes if an object has had it's values changed

		Args:
			indexed_object (IndexedObject): Object to parse and re-index
			remove (bool): If the object is to be removed from the Collection
		"""
		self.__lock.acquire()
		obj_id = indexed_object.id
		if isinstance(obj_id, str):
			obj_id = int(obj_id.split(COLLECTION_SEPARATOR, 1)[1])
		if remove and obj_id in self._collections:
			del self._collections[obj_id]

		values = [
			(a[:-2] if a.endswith(FIELD_SEPARATOR + '*') else a, b)
			for a, b in get_key_value(indexed_object)
		]
		keys = set([
			a[0][:-2] if a[0].endswith(FIELD_SEPARATOR + '*') else a[0] for a in values
		])
		verified_keys = {}

		for key in self.__indexes:
			if key not in keys:
				continue

			field_values = set([b for a, b in values if a == key])
			delete_fields = []

			for value in self.__indexes[key]:
				if remove or value not in field_values:
					if obj_id in self.__indexes[key][value]:
						self.__indexes[key][value].remove(obj_id)
				elif value in field_values and obj_id not in self.__indexes[key][value]:
					if key in self.__unique_fields and value is not None and len(self.__indexes[key][value]) == 1:
						old = self._collections[list(self.__indexes[key][value])[0]]
						raise UniqueIndexException(
							'Unique Field Constraint Error: multiple values for {0}: {1}'
							'\n\tCurrent Unique Values: {2}\n\tNew Unique Values: {3}'.format(
								key, value,
								"({0})".format(", ".join([
									"{0}: {1}".format(a, repr(b))
									for a, b in get_key_value(old) if a in self.__unique_fields
								])),
								"({0})".format(", ".join([
									"{0}: {1}".format(a, repr(b))
									for a, b in get_key_value(indexed_object)
									if a in self.__unique_fields
								]))
							))
					field_values.remove(value)
					if key not in verified_keys:
						verified_keys[key] = set()
					verified_keys[key].add(value)

				if len(self.__indexes[key][value]) == 0:
					delete_fields.append(value)
			for value in delete_fields:
				del self.__indexes[key][value]

			if not remove:
				for v in field_values:
					if key not in verified_keys:
						verified_keys[key] = set()
					verified_keys[key].add(v)

		for key in verified_keys:
			for value in verified_keys[key]:
				if value not in self.__indexes[key]:
					self.__indexes[key][value] = set()
				self.__indexes[key][value].add(obj_id)
		if not remove:
			self._collections[obj_id] = indexed_object
			if self.__iter_keys is not None:
				self.__iter_keys.append(obj_id)
		self.__lock.release()

	def add_unique_fields(self, *field_names):
		unique_fields = self.__unique_fields | set(field_names)
		for field_name in unique_fields:
			for value in self.__indexes[field_name]:
				if len(self.__indexes[field_name][value]) > 1:
					raise IndexError(
						'Unique Field Constraint Error: multiple values for {0}:{1}'
						''.format(field_name, value))
		self.__unique_fields = unique_fields

	def get(self, object_id):
		object_id = int(object_id)
		if object_id not in self._collections:
			return None
		return self._collections[object_id]

	def add_fields(self, *field_names):
		self.__other_fields = self.__other_fields | set(field_names)

	def add(self, new_object):
		self.__lock.acquire()

		while self.__max_id in self._collections:
			self.__max_id += 1
		new_obj = IndexedObject(new_object, self, self.__max_id)

		try:
			self.update_indexes_for_object(new_obj)
		except UniqueIndexException as e:
			self.__max_id -= 1
			raise e

		self.__lock.release()

	def remove(self, *queries):
		self.__lock.acquire()
		current_ids = self._get_ids(queries)
		if len(current_ids) == 0:
			return

		for object_id in current_ids:
			self.update_indexes_for_object(self._collections[object_id], remove=True)
		self.__lock.release()

	def _get_ids(self, queries):
		self.__lock.acquire()

		if len(queries) == 0:
			return list(self._collections.values())
		if len(queries) == 1 and len(queries[0]) > 0:
			queries = queries[0].split(' ', 2)
		if len(queries) == 3 and queries[0] in self.__unique_fields and queries[1] == '==':
			if queries[0] in self.__indexes and queries[2] in self.__indexes[queries[0]] and \
					len(self.__indexes[queries[0]][queries[2]]) > 0:
				return [list(self.__indexes[queries[0]][queries[2]])[0]]

		user_ids = []
		for f in filters.generate_filters(queries):
			test_val = None
			if len(f) == 2:
				col, op = f
			else:
				col, op, test_val = f

			if 'exists' in op:
				return (op == 'exists' and col in self.__indexes) or (
						op == '!exists' and col not in self.__indexes)

			if col not in self.__indexes:
				return False

			test_operator = filters.prepare_test(op, test_val)
			new_ids = [
				self.__indexes[col][a] for a in self.__indexes[col]
				if test_operator(a) is True]
			if len(new_ids) > 0:
				if isinstance(user_ids, list):
					user_ids = set.union(*new_ids)
				else:
					user_ids &= set.union(*new_ids)
		self.__lock.release()
		return user_ids

	def find(self, *queries):
		if len(queries) == 0:
			return self._collections.values()
		return [self._collections[k] for k in self._get_ids(queries)]

	def is_unique(self, field):
		return all([len(self.__indexes[field][a]) == 1 for a in self.__indexes[field]])

	def __contains__(self, item):
		return any([item in self.__indexes[a] for a in self.__unique_fields])

	def __iter__(self):
		self.__lock.acquire()
		self.__iter_keys = list(self._collections)
		self.__lock.release()
		self.__iter_idx = 0
		return self

	def next(self):
		self.__iter_idx += 1
		if self.__iter_idx >= len(self.__iter_keys):
			raise StopIteration()
		if self.__iter_keys[self.__iter_idx] not in self._collections:
			return self.next()
		return self._collections[self.__iter_keys[self.__iter_idx]]

	def __len__(self):
		return len(self._collections)


class ParentCollection(CollectionType):
	def __init__(self, parent=None, name=None):
		super(ParentCollection, self).__init__(parent, name)
		self.__current_collection = None

	def find(self, *queries):
		pass

	def collection(self, name, object_type=None):
		"""

		Args:
			name (str): Name to apply to new Collection
			object_type: Type of Collection to use. If None provided, the default type is used

		Returns:
			(IndexedCollection|ParentCollection): Collection of the base type added
		"""
		if COLLECTION_SEPARATOR in name:
			raise ValueError('Name of Collection may not contain a Field Separator Character')
		name = name.replace(' ', '_')
		if name not in self._collections:
			self._collections[name] = IndexedCollection(self, name) if object_type is None \
				else object_type(self, name)
		return self._collections[name]

	def get(self, object_id):
		p, i = object_id.split(COLLECTION_SEPARATOR, 1)
		return self.collection(p).get(i)

	def is_unique(self, field):
		p, i = field.split(COLLECTION_SEPARATOR, 1)
		if p == '*':
			names = self._collections.keys()
		elif p.startswith('[') and p.endswith(']'):
			names = p[1:-1].replace(' ', '').split(',')
		else:
			names = [p]
		return all([self.collection(k).is_unique(i) for k in names])
