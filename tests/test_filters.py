import unittest as ut
from datetime import datetime, timedelta


class _FilterBase(ut.TestCase):
	@classmethod
	def setUpClass(cls):
		import filters
		cls.f = filters
		if cls is _FilterBase:
			raise ut.SkipTest("Skip _FilterBase tests, it's a base class")
		super(_FilterBase, cls).setUpClass()

	def test_equal(self):
		raise NotImplementedError()

	def test_not_equal(self):
		raise NotImplementedError()

	def test_less_than(self):
		raise NotImplementedError()

	def test_less_equal(self):
		raise NotImplementedError()

	def test_greater_than(self):
		raise NotImplementedError()

	def test_greater_equal(self):
		raise NotImplementedError()

	def test_like(self):
		raise NotImplementedError()

	def test_contains(self):
		raise NotImplementedError()

	def test_not_contains(self):
		raise NotImplementedError()


class TestStringFilter(_FilterBase):
	def test_less_than(self):
		self.assertRaises(TypeError, self.f.query, ('str', '<', 'string'))

	def test_less_equal(self):
		self.assertRaises(TypeError, self.f.query, ('str', '<=', 'string'))

	def test_greater_than(self):
		self.assertRaises(TypeError, self.f.query, ('str', '>', 'string'))

	def test_greater_equal(self):
		self.assertRaises(TypeError, self.f.query, ('str', '>=', 'string'))

	def test_like(self):
		self.assertTrue(self.f.query('string', '~=', 'string'))
		self.assertTrue(self.f.query('strong', '~=', 'string'))
		self.assertFalse(self.f.query('tron', '~=', 'string'))
		self.assertFalse(self.f.query('abc', '~=', 'string'))

	def test_contains(self):
		self.assertTrue(self.f.query('str', 'in', 'string'))
		self.assertFalse(self.f.query('str1', 'in', 'string'))

	def test_not_contains(self):
		self.assertTrue(self.f.query('str1', '!in', 'string'))
		self.assertFalse(self.f.query('str', '!in', 'string'))

	def test_not_equal(self):
		self.assertFalse(self.f.query('str', '!=', 'str'))
		self.assertTrue(self.f.query('str1', '!=', 'str'))

	def test_equal(self):
		self.assertTrue(self.f.query('str', '==', 'str'))
		self.assertFalse(self.f.query('str1', '==', 'str'))

	def test_is_none(self):
		self.assertTrue(self.f.query(None, 'is None'))
		self.assertTrue(self.f.query(None, 'is null'))
		self.assertFalse(self.f.query('', 'is None'))
		self.assertFalse(self.f.query(1, 'is None'))


class TestIntegerFilter(_FilterBase):
	def test_less_than(self):
		self.assertTrue(self.f.query(1, '<', 2))
		self.assertFalse(self.f.query(2, '<', 1))
		self.assertFalse(self.f.query(2, '<', 2))

	def test_less_equal(self):
		self.assertTrue(self.f.query(1, '<=', 2))
		self.assertFalse(self.f.query(2, '<=', 1))
		self.assertTrue(self.f.query(2, '<=', 2))

	def test_greater_than(self):
		self.assertFalse(self.f.query(1, '>', 2))
		self.assertTrue(self.f.query(2, '>', 1))
		self.assertFalse(self.f.query(2, '>', 2))

	def test_greater_equal(self):
		self.assertFalse(self.f.query(1, '>=', 2))
		self.assertTrue(self.f.query(2, '>=', 1))
		self.assertTrue(self.f.query(2, '>=', 2))

	def test_like(self):
		self.assertRaises(TypeError, self.f.query, (1, '~=', 3))

	def test_contains(self):
		self.assertRaises(TypeError, self.f.query, (1, 'in', 3))

	def test_not_contains(self):
		self.assertRaises(TypeError, self.f.query, (1, '!in', 3))

	def test_not_equal(self):
		self.assertFalse(self.f.query(3, '!=', 3))
		self.assertTrue(self.f.query(2, '!=', 3))

	def test_equal(self):
		self.assertTrue(self.f.query(2, '==', 2))
		self.assertFalse(self.f.query(2, '==', 1))


class TestFloatFilter(_FilterBase):
	def test_equal(self):
		self.assertTrue(self.f.query(1.1, '==', 1.1))
		self.assertTrue(self.f.query(1., '==', 1))
		self.assertFalse(self.f.query(1., '==', 1.1))
		self.assertFalse(self.f.query(1.1, '==', 1.099999999))
		self.assertFalse(self.f.query(1.1, '==', 1.11))

	def test_not_equal(self):
		self.assertFalse(self.f.query(1.1, '!=', 1.1))
		self.assertFalse(self.f.query(1., '!=', 1))
		self.assertTrue(self.f.query(1., '!=', 1.1))
		self.assertTrue(self.f.query(1.1, '!=', 1.099999999))
		self.assertTrue(self.f.query(1.1, '!=', 1.11))

	def test_less_than(self):
		self.assertTrue(self.f.query(1.1, '<', 1.2))
		self.assertTrue(self.f.query(1.1, '<', 1.11))
		self.assertTrue(self.f.query(1., '<', 1.1))
		self.assertTrue(self.f.query(0.9, '<', 1))
		self.assertTrue(self.f.query(0, '<', 1))
		self.assertFalse(self.f.query(1., '<', 1.))
		self.assertFalse(self.f.query(1.1, '<', 1))

	def test_less_equal(self):
		self.assertTrue(self.f.query(1.1, '<=', 1.2))
		self.assertTrue(self.f.query(1.1, '<=', 1.11))
		self.assertTrue(self.f.query(1., '<=', 1.1))
		self.assertTrue(self.f.query(0.9, '<=', 1))
		self.assertTrue(self.f.query(0, '<=', 1))
		self.assertTrue(self.f.query(1., '<=', 1.))
		self.assertFalse(self.f.query(1.1, '<=', 1))

	def test_greater_than(self):
		self.assertFalse(self.f.query(1.1, '>', 1.2))
		self.assertFalse(self.f.query(1.1, '>', 1.11))
		self.assertFalse(self.f.query(1., '>', 1.1))
		self.assertFalse(self.f.query(0.9, '>', 1))
		self.assertFalse(self.f.query(0, '>', 1))
		self.assertFalse(self.f.query(1., '>', 1.))
		self.assertTrue(self.f.query(1.1, '>', 1))

	def test_greater_equal(self):
		self.assertFalse(self.f.query(1.1, '>=', 1.2))
		self.assertFalse(self.f.query(1.1, '>=', 1.11))
		self.assertFalse(self.f.query(1., '>=', 1.1))
		self.assertFalse(self.f.query(0.9, '>=', 1))
		self.assertFalse(self.f.query(0, '>=', 1))
		self.assertTrue(self.f.query(1., '>=', 1.))
		self.assertTrue(self.f.query(1.1, '>=', 1))

	def test_like(self):
		self.assertRaises(TypeError, self.f.query, (1.1, '~=', 2.1))

	def test_contains(self):
		self.assertRaises(TypeError, self.f.query, (1.1, 'in', 2.1))

	def test_not_contains(self):
		self.assertRaises(TypeError, self.f.query, (1.1, '!in', 2.1))


class TestDateTimeFilter(_FilterBase):
	def test_equal(self):
		t1 = datetime.now()
		t2 = datetime.now() - timedelta(minutes=4)
		self.assertTrue(self.f.query(t1, '==', t1))
		self.assertFalse(self.f.query(t1, '==', t2))

	def test_not_equal(self):
		t1 = datetime.now()
		t2 = datetime.now() - timedelta(minutes=4)
		self.assertFalse(self.f.query(t1, '!=', t1))
		self.assertTrue(self.f.query(t1, '!=', t2))

	def test_less_than(self):
		t1 = datetime.now()
		t2 = datetime.now() + timedelta(minutes=4)
		self.assertFalse(self.f.query(t1, '<', t1))
		self.assertTrue(self.f.query(t1, '<', t2))
		self.assertFalse(self.f.query(t2, '<', t1))

	def test_less_equal(self):
		t1 = datetime.now()
		t2 = datetime.now() + timedelta(minutes=4)
		self.assertTrue(self.f.query(t1, '<=', t1))
		self.assertTrue(self.f.query(t1, '<=', t2))
		self.assertFalse(self.f.query(t2, '<=', t1))

	def test_greater_than(self):
		t1 = datetime.now()
		t2 = datetime.now() + timedelta(minutes=4)
		self.assertFalse(self.f.query(t1, '>', t1))
		self.assertFalse(self.f.query(t1, '>', t2))
		self.assertTrue(self.f.query(t2, '>', t1))

	def test_greater_equal(self):
		t1 = datetime.now()
		t2 = datetime.now() + timedelta(minutes=4)
		self.assertTrue(self.f.query(t1, '>=', t1))
		self.assertFalse(self.f.query(t1, '>=', t2))
		self.assertTrue(self.f.query(t2, '>=', t1))

	def test_like(self):
		t1 = datetime.now()
		t2 = datetime.now() + timedelta(minutes=4)
		self.assertRaises(TypeError, self.f.query, (t1, '~=', t2))

	def test_contains(self):
		t1 = datetime.now()
		t2 = datetime.now() + timedelta(minutes=4)
		self.assertRaises(TypeError, self.f.query, (t1, 'in', t2))

	def test_not_contains(self):
		t1 = datetime.now()
		t2 = datetime.now() - timedelta(minutes=4)
		self.assertRaises(TypeError, self.f.query, (t1, '!in', t2))


if __name__ == '__main__':
	ut.main()
