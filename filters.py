import datetime
import jellyfish
import six


class _EmptyField(object):
	pass


EMPTY = _EmptyField()
OPERATORS = {
	'in': (tuple(list(six.string_types) + [list, dict]), lambda x, y: x in y),
	'!in': (tuple(list(six.string_types) + [list, dict]), lambda x, y: x not in y),
	'==': (
		tuple(list(six.string_types) + [int, float, datetime.datetime, datetime.timedelta]),
		lambda x, y: x == y,
	),
	'!=': (
		tuple(list(six.string_types) + [int, float, datetime.datetime, datetime.timedelta]),
		lambda x, y: x != y,
	),
	'>': (
		(int, float, datetime.datetime, datetime.timedelta),
		lambda x, y: x > y,
	),
	'>=': (
		(int, float, datetime.datetime, datetime.timedelta),
		lambda x, y: x >= y,
	),
	'<': (
		(int, float, datetime.datetime, datetime.timedelta),
		lambda x, y: x < y,
	),
	'<=': (
		(int, float, datetime.datetime, datetime.timedelta),
		lambda x, y: x <= y,
	),
	'~=': (
		six.string_types, lambda x, y: jellyfish.jaro_distance(six.u(x), six.u(y)) > 0.82
	),
	'is None': (
		(object, ), lambda x, y: x is None
	),
	'is null': (
		(object, ), lambda x, y: x is None
	),
	'not None': (
		(object, ), lambda x, y: x is not None
	),
	'not null': (
		(object, ), lambda x, y: x is not None
	)
}


def add_operator(operator, types, test_evaluator):
	OPERATORS[operator] = (types, test_evaluator)


def generate_filters(*filters):
	"""Converts input list of filters into a sequence of parsed and prepared queries

	Args:
		filters (str|(str, str, str))[]: List of queries to be parsed

	Yields:
		(str, str, str)|(str, str): List of parsed filters
	"""

	new_filters = []
	for f in filters:
		if isinstance(f, (tuple, list)):
			for a in generate_filters(f):
				yield a
		elif isinstance(f, (six.string_types, int, float)):
			new_filters.append(f)
		if len(new_filters) == 3 or (len(new_filters) == 2 and 'exists' in new_filters[1]):
			yield tuple(new_filters)
			new_filters = []

	if len(new_filters) > 1 and (('exists' in new_filters[-1] and len(new_filters) == 2) or (len(new_filters) == 3)):
		yield tuple(new_filters)


def _schema_test(operator, test_value):
	return isinstance(test_value, OPERATORS[operator][0])


def prepare_test(operator, test_value):
	"""Performs evaluation of the given query

	Args:
		operator (str): Operator to perform test
		test_value: Evaluation Test Value (right of operand)

	Returns:
		func: Prepared Test Function to test field values
	"""
	if operator not in OPERATORS:
		raise ValueError("Given Operator '{}' is not supported".format(operator))
	if not _schema_test(operator, test_value):
		raise TypeError("Test Value {0} not supported with this operation '{1}'".format(repr(test_value), operator))

	def _do_test(input_value):
		if not _schema_test(operator, input_value):
			raise TypeError("Field Value {0} not supported with this operation '{1}'".format(
				repr(input_value), operator))
		return OPERATORS[operator][1](input_value, test_value)
	return _do_test


def query(field_value, operator, test_value=EMPTY):
	"""Parses incoming query and returns a valid condition of the result

	Args:
		field_value: Value to be tested
		operator (str): Operator Identifier of the operation to perform
		test_value: Value to test if the condition were to succeed

	Notes:
		- if `test_value` is not provided,

	Returns:
		bool: If the query returns successfully
	"""
	test = prepare_test(operator, test_value)
	return test(field_value)
